package es.fransoto.versionmanager

/**
 * Created by franciscojosesotoportillo on 14/3/17.
 */
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.util.Log
import org.json.JSONArray
import org.json.JSONException
import java.util.*

class VersionManager private constructor(
        context: Context,
        updateCallback: (lastVersion: String, currentVersion: String) -> Unit,
        firstInstallCallback: () -> Unit
) {

    companion object {
        private const val TAG = "VersionManager"
        private const val DEFAULT_VERSION_NAME = "0.0"
        private var instance: VersionManager? = null
        private const val PREVIOUS_VERSION_KEY = "vm_previous_version"
        private const val PREVIOUS_VERSION_CODE_KEY = "vm_previous_version_code"
        private const val CURRENT_VERSION_KEY = "vm_current_version"
        private const val CURRENT_VERSION_CODE_KEY = "vm_current_version_code"
        private const val FIRST_LAUNCH_KEY = "vm_first_launch"
        private const val LAUNCHES_COUNT_KEY = "vm_launches_count"
        private const val INSTALLED_VERSIONS_KEY = "vm_installed_versions"

        @JvmStatic
        fun getInstance(
                context: Context,
                updateCallback: (lastVersion: String, currentVersion: String) -> Unit = { _: String, _: String -> },
                firstInstallCallback: () -> Unit = {}
        ): VersionManager {
            return instance ?: VersionManager(context, updateCallback, firstInstallCallback).also { instance = it }
        }
    }

    private var versionCode = 0
    private val previousVersionCode: Int
    private var currentVersionName: String = DEFAULT_VERSION_NAME
    private val previousVersionName: String
    var isFirstInstall = false
        private set
    var isAnUpdate = false
        private set
    private val preferences: SharedPreferences?

    fun getCurrentVersionName() = preferences?.getString(CURRENT_VERSION_KEY, DEFAULT_VERSION_NAME) ?: DEFAULT_VERSION_NAME
    fun getPreviousVersionName() = preferences?.getString(PREVIOUS_VERSION_KEY, DEFAULT_VERSION_NAME) ?: DEFAULT_VERSION_NAME
    fun launchesCount() = preferences?.getInt(LAUNCHES_COUNT_KEY, 0) ?: 0

    //nothing
    val installedVersions: List<String>
        get() = readInstalledVersions()

    val firstLaunchDate: Date?
        get() = if (preferences != null) {
            val time: Long = preferences.getLong(FIRST_LAUNCH_KEY, 0)
            Date(time)
        } else {
            null
        }

    init {
        try {
            currentVersionName = context.packageManager
                    .getPackageInfo(context.packageName, 0).versionName
        } catch (nfe: PackageManager.NameNotFoundException) {
            Log.e(TAG, "can't find version name")
        }
        try {
            versionCode = context.packageManager
                    .getPackageInfo(context.packageName, 0).versionCode
        } catch (nfe: PackageManager.NameNotFoundException) {
            Log.e(TAG, "can't find version code")
        }

        preferences = context.getSharedPreferences(TAG, Context.MODE_PRIVATE)
        val editor: SharedPreferences.Editor = preferences.edit()
        updateCurrentVersion(editor, preferences)

        previousVersionCode = preferences.getInt(CURRENT_VERSION_CODE_KEY, 0)
        previousVersionName = preferences.getString(CURRENT_VERSION_KEY, "") ?: ""

        if (previousVersionName == "") {
            isFirstInstall = true
            firstInstallCallback.invoke()
            val current = Date()
            editor.putLong(FIRST_LAUNCH_KEY, current.time)

            val jsonArray = JSONArray()
            updateInstalledVersions(jsonArray, editor)
        } else if (previousVersionName != currentVersionName) {
            isAnUpdate = true
            updateCallback.invoke(previousVersionName, currentVersionName)

            updatePreviousVersion(editor)

            val jsonArray = readInstalledVersions(preferences)
            updateInstalledVersions(jsonArray, editor)
        }
        editor.apply()
    }

    private fun updatePreviousVersion(editor: SharedPreferences.Editor) {
        editor.putString(PREVIOUS_VERSION_KEY, previousVersionName)
        editor.putInt(PREVIOUS_VERSION_CODE_KEY, previousVersionCode)
    }

    private fun updateInstalledVersions(jsonArray: JSONArray?, editor: SharedPreferences.Editor) {
        jsonArray?.put(currentVersionName)
        editor.putString(INSTALLED_VERSIONS_KEY, jsonArray.toString())
    }

    private fun readInstalledVersions(preferences: SharedPreferences): JSONArray? {
        var jsonArray: JSONArray? = null
        try {
            jsonArray = JSONArray(preferences.getString(INSTALLED_VERSIONS_KEY, "[]"))
        } catch (e: JSONException) {
            Log.e(TAG, "can't load Installed versions")
        }
        return jsonArray
    }

    private fun updateCurrentVersion(editor: SharedPreferences.Editor, preferences: SharedPreferences) {
        editor.putString(CURRENT_VERSION_KEY, currentVersionName)
        editor.putInt(CURRENT_VERSION_CODE_KEY, versionCode)
        val launchesCount: Int = preferences.getInt(LAUNCHES_COUNT_KEY, 0).inc()
        editor.putInt(LAUNCHES_COUNT_KEY, launchesCount)
    }

    private fun readInstalledVersions(): List<String> {
        val installedArray: MutableList<String> = ArrayList()
        return if (preferences != null) {
            var jsonArray: JSONArray? = null
            try {
                jsonArray = JSONArray(preferences.getString(INSTALLED_VERSIONS_KEY, "[]"))
            } catch (e: JSONException) {
                Log.e(TAG, "can't load Installed versions")
            }
            if (jsonArray != null) {
                for (i in 0 until jsonArray.length()) {
                    try {
                        installedArray.add(jsonArray.getString(i))
                    } catch (jse: JSONException) {
                        //nothing
                        Log.e(TAG, "Can't load Installed versions")
                    }
                }
            }
            installedArray
        } else {
            installedArray
        }
    }
}