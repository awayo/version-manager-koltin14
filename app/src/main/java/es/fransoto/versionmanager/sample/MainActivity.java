package es.fransoto.versionmanager.sample;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.List;

import es.fransoto.versionmanager.VersionManager;
import kotlin.Unit;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        VersionManager vm = VersionManager.getInstance(this,
                (previous, current) -> {
                    if (previous.startsWith("1.") && Double.parseDouble(current) >= 2.0) {
                        fire1to2Migration();
                    }
                    return Unit.INSTANCE;
                },
                () -> {
                    return Unit.INSTANCE;
                }
        );

        if (vm.isFirstInstall()) {
            Log.d("LOG", "FIRST INSTALL");
            Log.d("LOG", vm.getFirstLaunchDate().toString());
        }

        if (vm.isAnUpdate()) {
            Log.d("LOG", "IS UPDATE");
        }

        Log.d("LOG", vm.getCurrentVersionName());
        Log.d("LOG", "{");

        List<String> array = vm.getInstalledVersions();

        for (int i = 0 ; i < array.size(); i++) {
            Log.d("LOG", array.get(i));
        }

        Log.d("LOG", "}");
    }

    private void fire1to2Migration() {
        Toast.makeText(this, "Fire Migration from version 1 to 2", Toast.LENGTH_LONG).show();
    }
}
