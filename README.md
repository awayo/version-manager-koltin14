# VersionManager #

Librería para llevar el control de las versiones instaladas por el usuario, detectar si es la primera instalación o saber sobre que versión está actualizando.

### Instalación ###


Añade las siguientes entradas en build.gradle:

```java
   repositories {
       ...
       maven { url "https://jitpack.io" }
   }
```

```java

    compile "org.bitbucket.awayo:VersionManagerAndroid:v1.0.2"
```

### Uso ###

```java

   public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        VersionManager vm = VersionManager.getInstance(this,
                (previous, current) -> {
                    if (previous.startsWith("1.") && Double.parseDouble(current) >= 2.0) {
                        fire1to2Migration();
                    }
                    return Unit.INSTANCE;
                },
                () -> {
                    return Unit.INSTANCE;
                }
        );

        if (vm.isFirstInstall()) {
            Log.d("LOG", "FIRST INSTALL");
            Log.d("LOG", vm.getFirstLaunchDate().toString());
        }

        if (vm.isAnUpdate()) {
            Log.d("LOG", "IS UPDATE");
        }

        Log.d("LOG", vm.getCurrentVersionName());
    }
}
```